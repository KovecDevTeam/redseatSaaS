package com.SaaS.redseat.model;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.DutyList;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.ExRateHistory;

public class ShippingCalculator {

	static final double NO_OF_DECIMAL = 100;

	static final double STAMP_FEE = 100;

	static final double CAF_USER = 2500;

	static final double CAF_COMPANY = 5000;

	private double duty;
	private double scf;
	private double envl;
	private double caf;
	private double gct;
	private double stamp;
	private double icd;
	private double freight;
	private double customs;

	public double getDuty() {
		return duty;
	}

	public void setDuty(double duty) {
		this.duty = duty;
	}

	public double getScf() {
		return Math.round(scf * NO_OF_DECIMAL) / NO_OF_DECIMAL;
	}

	public void setScf(double scf) {
		this.scf = scf;
	}

	public double getEnvl() {

		return Math.round(envl * NO_OF_DECIMAL) / NO_OF_DECIMAL;
	}

	public void setEnvl(double envl) {
		this.envl = envl;

	}

	public double getCaf() {
		return Math.round(caf * NO_OF_DECIMAL) / NO_OF_DECIMAL;
	}

	public void setCaf(double caf) {
		this.caf = caf;
	}

	public double getGct() {
		return Math.round(gct * NO_OF_DECIMAL) / NO_OF_DECIMAL;

	}

	public void setGct(double gct) {
		this.gct = gct;
	}

	public double getStamp() {
		return stamp;
	}

	public void setStamp(double stamp) {
		this.stamp = stamp;
	}

	public double getIcd() {

		return Math.round(icd * NO_OF_DECIMAL) / NO_OF_DECIMAL;
	}

	public void setIcd(double icd) {
		this.icd = icd;
	}

	public double getFreight() {

		return Math.round(freight * NO_OF_DECIMAL) / NO_OF_DECIMAL;
	}

	public void setFreight(double freight) {
		this.freight = freight;
	}

	public double getCustoms() {

		return Math.round(customs * NO_OF_DECIMAL) / NO_OF_DECIMAL;

	}

	public void setCustoms(double customs) {
		this.customs = customs;
	}

	public ShippingCalculator calculate(DutyList dutyList, double cost, double freightFee, double processingFee,
			ExRateHistory rate, boolean isCompany) {

		ShippingCalculator finalcost = new ShippingCalculator();

		if (cost > 50.00) {
			final double ItemPrice = ((cost * rate.getExRate()) + (freightFee + processingFee));

			finalcost.setDuty(ItemPrice * dutyList.getDuty());
			finalcost.setEnvl(ItemPrice * 0.005);
			finalcost.setScf(ItemPrice * 0.003);
			finalcost.setCaf(isCompany ? CAF_USER : CAF_COMPANY);
			finalcost.setStamp(STAMP_FEE);

			final double totalTax = (finalcost.getCaf() + finalcost.getEnvl() + finalcost.getDuty() + finalcost.getScf()
					+ finalcost.getStamp());

			finalcost.setDuty(ItemPrice * dutyList.getDuty());

			finalcost.setGct(ItemPrice * dutyList.getGct());

			final double itemTotal = (finalcost.getGct() + totalTax);

			finalcost.setCustoms(itemTotal);
		}

		finalcost.setFreight(freightFee + processingFee);

		return finalcost;
	}

}
