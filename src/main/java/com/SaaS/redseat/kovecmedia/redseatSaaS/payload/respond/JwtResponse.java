
package com.SaaS.redseat.kovecmedia.redseatSaaS.payload.respond;

import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String email;
	private List<String> roles;
	private long companyId;

	public JwtResponse(String accessToken, Long id, String username, String email, List<String> roles,long companyId) {
		this.token = accessToken;
		this.id = id;
		this.email = email;
		this.roles = roles;
		this.companyId =companyId;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public List<String> getRoles() {
		return roles;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	
}
	