package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Identification;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IdentificationRepo extends JpaRepository<Identification,Long> {
    
}
