package com.SaaS.redseat.kovecmedia.redseatSaaS.service;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Country;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.Payload;

public interface CountryService {
    
    public Payload<Country>  getAllCounty();

}