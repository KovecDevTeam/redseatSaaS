package com.SaaS.redseat.kovecmedia.redseatSaaS.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.validation.Valid;

import com.SaaS.redseat.kovecmedia.redseatSaaS.constant.GlobalCache;
import com.SaaS.redseat.kovecmedia.redseatSaaS.constant.StautsCode;
import com.SaaS.redseat.kovecmedia.redseatSaaS.jwt.JwtUtils;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.Payload;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request.LoginRequest;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request.UserRegistrationRequest;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.respond.JwtResponse;
import com.SaaS.redseat.kovecmedia.redseatSaaS.publisher.UserRegisterEventPublisher;
import com.SaaS.redseat.kovecmedia.redseatSaaS.security.service.UserDetailsImpl;
import com.SaaS.redseat.kovecmedia.redseatSaaS.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService service;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private UserRegisterEventPublisher userRegisterEventPublisher;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        try {
            String companyId =String.valueOf(loginRequest.getCompanyId());

            GlobalCache.putCache("CompanyID", companyId);
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword().concat(companyId) ));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);
               
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            userDetails.setCompanyId(loginRequest.getCompanyId());

            List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
                    .collect(Collectors.toList());

            return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
                    userDetails.getEmail(), roles, userDetails.getCompanyId()));
        } catch (BadCredentialsException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

    }

    @PostMapping("/signup")
    public @Valid Payload<UserRegistrationRequest> registerUser(
            @Valid @RequestBody UserRegistrationRequest signUpRequest) {

        Payload<UserRegistrationRequest> payload = new Payload<>();

        if ((Boolean.TRUE
                .equals(service.existsByEmailAndCompany(signUpRequest.getEmail(), signUpRequest.getCompanyId())))) {

            payload.setStatus(false);
            payload.setCode(StautsCode.RCS_103);
            payload.setMessage("Error: Email is already in use!");
            payload.setData(null);
        } else {

            userRegisterEventPublisher.publishEvent(signUpRequest);

            payload.setStatus(true);
            payload.setCode(StautsCode.RCS_103);
            payload.setMessage("User Created");
            payload.setData(null);

        }

        return payload;

    }

}
