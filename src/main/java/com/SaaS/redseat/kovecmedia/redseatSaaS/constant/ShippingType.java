package com.SaaS.redseat.kovecmedia.redseatSaaS.constant;

public enum ShippingType {
    AIR, OCEAN
}
