package com.SaaS.redseat.kovecmedia.redseatSaaS.provider;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import com.SaaS.redseat.kovecmedia.redseatSaaS.constant.GlobalCache;
import com.SaaS.redseat.kovecmedia.redseatSaaS.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    UserService service;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        String company = "";
        // Access global variable
        Map<String, String> map = GlobalCache.cache;
        for (Entry<String, String> entry : map.entrySet()) {
            if (entry.getKey().equals("CompanyID")) {
                company = entry.getValue();
                break;
            }

        }
        if ((Boolean.TRUE.equals(service.existsByEmailAndCompany(name, Long.parseLong(company))))) {
            return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
        } else {
            return null;
        }

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}