package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Shipment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentRepo  extends JpaRepository<Shipment, Long> {
    
}