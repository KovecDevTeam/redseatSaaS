package com.SaaS.redseat.kovecmedia.redseatSaaS.controller;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.User;
import com.SaaS.redseat.kovecmedia.redseatSaaS.publisher.UserRegisterEventPublisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {

   
    
    @GetMapping(value = "/", produces = "application/json")
    public  @ResponseBody User index() {
    
        User newuser = new User();

        newuser.setName("TEst");
        newuser.setIsActive(false);
         
       
      return  newuser;
    }


  
}
