package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.PackageStatus;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PackageStatusRepo  extends JpaRepository<PackageStatus, Long> {
    
}
