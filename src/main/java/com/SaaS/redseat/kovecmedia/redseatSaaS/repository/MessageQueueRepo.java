package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.MessageQueue;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageQueueRepo extends JpaRepository<MessageQueue, Long> {
    
}
