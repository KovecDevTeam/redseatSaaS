package com.SaaS.redseat.kovecmedia.redseatSaaS.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Entity
@Table(name = "users")
@SequenceGenerator(name = "user_id_seq", allocationSize = 1)
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq")
	private Long id;

	@Column(name = "points", insertable = false, columnDefinition = "BIGINT DEFAULT 0")
	private Long points;

	private String name;

	private String password;

	private String trn;

	private String referrer;

	@OneToOne(fetch = FetchType.LAZY)
	private Branch city;

	@Column(name = "isActive", insertable = false, updatable = true, columnDefinition = "boolean DEFAULT true")
	private Boolean isActive;

	@Column(name = "isLocked", insertable = false, updatable = true, columnDefinition = "boolean DEFAULT true")
	private Boolean isLocked;

	private String email;

	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
	@JoinTable(name = "users_phone", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "contactnumber_id") })
	private Set<ContactNumber> phone;

	@Transient
	private String passwordConfirm;

	@OneToMany(fetch = FetchType.LAZY)
	private Set<Address> address;

	@ManyToMany(fetch = FetchType.LAZY)
	private Set<Role> roles;

	@Column(name = "created_at", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")

	private java.sql.Timestamp createdAt;

	@Column(name = "updated_at", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")

	private java.sql.Timestamp updatedAt;

	private String updatedBy;

	@OneToOne(fetch = FetchType.EAGER)
	private Company company;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long points) {
		this.points = points;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = passwordEncoder().encode(password);
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public Set<Address> getAddress() {
		return address;
	}

	public void setAddress(Set<Address> address) {
		this.address = address;
	}

	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(java.sql.Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public java.sql.Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(java.sql.Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Set<ContactNumber> getPhone() {
		return phone;
	}

	public void setPhone(Set<ContactNumber> phone) {
		this.phone = phone;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getTrn() {
		return trn;
	}

	public void setTrn(String trn) {
		this.trn = trn;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public Branch getCity() {
		return city;
	}

	public void setCity(Branch city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "User [address=" + address + ", city=" + city + ", company=" + company + ", createdAt=" + createdAt
				+ ", email=" + email + ", id=" + id + ", isActive=" + isActive + ", isLocked=" + isLocked + ", name="
				+ name + ", password=" + password + ", passwordConfirm=" + passwordConfirm + ", phone=" + phone
				+ ", points=" + points + ", referrer=" + referrer + ", trn=" + trn + ", updatedAt=" + updatedAt
				+ ", updatedBy=" + updatedBy + "]";
	}

	public Boolean getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	

}