package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.DutyList;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DutyListRepo extends JpaRepository<DutyList, Long> {
    
}