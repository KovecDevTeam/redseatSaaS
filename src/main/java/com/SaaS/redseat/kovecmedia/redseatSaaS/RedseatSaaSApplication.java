package com.SaaS.redseat.kovecmedia.redseatSaaS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedseatSaaSApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedseatSaaSApplication.class, args);
	}

}
