package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Billing;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BillingRepo extends JpaRepository<Billing, Long> {
    
}