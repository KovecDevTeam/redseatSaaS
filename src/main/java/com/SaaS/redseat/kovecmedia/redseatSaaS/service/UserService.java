package com.SaaS.redseat.kovecmedia.redseatSaaS.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Company;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Role;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.User;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request.ResetRequest;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request.UserRegistrationRequest;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.respond.UserPackages;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService {
    public User getUser(long id);

	public List<User> getAllUsers();

	public UserPackages getUserPakages(long id);

	UserDetails loadUserByEmail(String email) throws UsernameNotFoundException;

	Boolean existsByEmail(String email);

	@Transactional
	void addUser(UserRegistrationRequest userRegistration);

	@Transactional
	void resetPassword(String email) throws UsernameNotFoundException;

	boolean checkToken(String token);
	
	void updatePassword(ResetRequest request);
	
	User updateProfile(User user);  

	Optional<User> findByEmailAndCompany(String email,long companyId);
	Boolean existsByEmailAndCompany(String email,long companyId);
}
