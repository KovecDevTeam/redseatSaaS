package com.SaaS.redseat.kovecmedia.redseatSaaS.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import com.SaaS.redseat.kovecmedia.redseatSaaS.constant.PhoneType;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Address;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Company;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.ContactNumber;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Role;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.User;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request.ResetRequest;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request.UserRegistrationRequest;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.respond.UserPackages;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.AddressRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.CompanyRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.ContactNumberRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.CountryRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.RoleRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.UserRepo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {

    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    UserRepo userRepository;
    @Autowired
    CompanyRepo companyRepo;
    @Autowired
    private AddressRepo addressRepository;
    @Autowired
    private ContactNumberRepo contactnumberepository;
    @Autowired
    private CountryRepo countryrepository;

    @Autowired
    private RoleRepo roleRepo;

    @Override
    @Transactional
    public void addUser(UserRegistrationRequest userRegistration) {
        User user = new User();
        Address address = new Address();
        ContactNumber contactNumber = new ContactNumber();
        Set<Address> addresslist = new HashSet<>();

        Set<ContactNumber> contactlist = new HashSet<>();

        Set<Role> rolelist = new HashSet<>();
        try {

            address.setAddressline1(userRegistration.getAddressLine1());
            address.setAddressline2(userRegistration.getAddressLine2());
            address.setZipcode(userRegistration.getZipCode());
            address.setCountry(countryrepository.getById((long) 1));

            addresslist.add(address);

            contactNumber.setIsprimary(true);
            contactNumber.setNumber(userRegistration.getPhone());
            contactNumber.setType(PhoneType.CELL);

            contactlist.add(contactNumber);

            rolelist.add( roleRepo.getById(userRegistration.getRoleId()) );

            user.setAddress(addresslist);
            user.setPhone(contactlist);
            user.setRoles(rolelist);

            user.setPassword(userRegistration.getPassword().concat(String.valueOf(userRegistration.getCompanyId())));
            user.setName(userRegistration.getName());
            user.setEmail(userRegistration.getEmail());
            user.setPoints((long) 0);
            
            companyRepo.findById(userRegistration.getRoleId());


            user.setCompany( companyRepo.getById(userRegistration.getCompanyId()));
            
            addressRepository.save(address);
            contactnumberepository.save(contactNumber);
            userRepository.save(user);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

    }

    @Override
    public boolean checkToken(String token) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Boolean existsByEmail(String email) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Boolean existsByEmailAndCompany(String email, long companyId) {
        // TODO Auto-generated method stub

        Optional<Company> company = companyRepo.findById(companyId);

        if (company.isPresent()) {
            return userRepository.existsByEmailAndCompany(email, company.get());
        } else {
            logger.error("Company ID Not Found:{}", companyId);
            return false;
        }
    }

    @Override
    public Optional<User> findByEmailAndCompany(String email, long companyId) {
        // TODO Auto-generated method stub

        Company company = companyRepo.findById(companyId).get();

        return userRepository.findByEmailAndCompany(email, company);
      
    }

    @Override
    public List<User> getAllUsers() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public User getUser(long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UserPackages getUserPakages(long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void resetPassword(String email) throws UsernameNotFoundException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updatePassword(ResetRequest request) {
        // TODO Auto-generated method stub

    }

    @Override
    public User updateProfile(User user) {
        // TODO Auto-generated method stub
        return null;
    }

}
