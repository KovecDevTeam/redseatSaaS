package com.SaaS.redseat.kovecmedia.redseatSaaS.service.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.ContactNumber;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sendinblue.ApiClient;
import sendinblue.ApiException;
import sendinblue.Configuration;
import sendinblue.auth.ApiKeyAuth;
import sibApi.ContactsApi;
import sibApi.TransactionalEmailsApi;
import sibModel.CreateContact;
import sibModel.GetSmtpTemplateOverview;
import sibModel.SendSmtpEmail;
import sibModel.SendSmtpEmailSender;
import sibModel.SendSmtpEmailTo;
import sibModel.SendTestEmail;

@Service
public class SendinBuleImpl implements SendinBuleAPI {

    Logger logger = LoggerFactory.getLogger(SendinBuleImpl.class);

     static final long RED_SEAT_CUSTOMER_LIST = 4;

    static final String LOGGER_MESSAGE = "API Exception occurred:-";

    ApiClient defaultClient = Configuration.getDefaultApiClient();
    ApiKeyAuth apiKey = (ApiKeyAuth) defaultClient.getAuthentication("api-key");

    @Override
    public boolean importContact(User user, String sendInBuleKey) {

        apiKey.setApiKey(sendInBuleKey);
        ContactsApi api = new ContactsApi();

        StringBuilder logMsg = new StringBuilder();
        try {

            String[] fname = user.getName().split(" ");

            ContactNumber sms = new ContactNumber();

            for (ContactNumber phone : user.getPhone()) {

                if (phone.getIsprimary()) {
                    sms = phone;
                }
            }

            CreateContact createContact = new CreateContact();
            createContact.setEmail(user.getEmail());
            Properties attributes = new Properties();
            attributes.setProperty("FIRSTNAME", fname[0]);
            attributes.setProperty("LASTNAME", fname[1]);
            attributes.setProperty("SMS", sms.getNumber());
            attributes.setProperty("RSC_CUSTOMER_NUMBER", "RSC" + user.getId());
            createContact.setAttributes(attributes);
            List<Long> listIds = new ArrayList<>();
            listIds.add(RED_SEAT_CUSTOMER_LIST);
            createContact.setListIds(listIds);
            createContact.setEmailBlacklisted(false);
            createContact.setSmsBlacklisted(false);
            createContact.setUpdateEnabled(false);

            api.createContact(createContact);

            logMsg.append(user.getName());
            logMsg.append("Import to Sending Bule for Company:");
            logMsg.append(user.getCompany().getName());

            logger.info("my message {}", logMsg);

            return true;

        } catch (ApiException e) {

            logger.info(LOGGER_MESSAGE);
            logger.error(" {}", e.getResponseBody());
            return false;

        } catch (Exception ex) {

            logger.error("{}", ex.getMessage());
            return false;
        }

    }

    @Override
    public boolean sendWelcomeEmail(long templateId, User user, String sendInBuleKey) {

        apiKey.setApiKey(sendInBuleKey);

        try {

            TransactionalEmailsApi api = new TransactionalEmailsApi();
            SendSmtpEmailSender sender = new SendSmtpEmailSender();
            sender.setEmail(user.getCompany().getSenderEmail());
            sender.setName(user.getCompany().getName());
            List<SendSmtpEmailTo> toList = new ArrayList<>();
            SendSmtpEmailTo to = new SendSmtpEmailTo();
            to.setEmail(user.getEmail());
            to.setName(user.getName());
            toList.add(to);
            SendSmtpEmail sendSmtpEmail = new SendSmtpEmail();
            sendSmtpEmail.setSender(sender);
            sendSmtpEmail.setTo(toList);

            sendSmtpEmail.setTemplateId(templateId);
            api.sendTransacEmail(sendSmtpEmail);

            return true;
        } catch (ApiException e) {

            logger.info(LOGGER_MESSAGE);
            logger.error(" {}", e.getResponseBody());
            return false;

        } catch (Exception ex) {
            logger.error("{}", ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean testEmailTemaple(long templateId, String email, String sendInBuleKey) {

        apiKey.setApiKey(sendInBuleKey);

        try {

            TransactionalEmailsApi api = new TransactionalEmailsApi();

            List<String> emailTo = new ArrayList<>();
            emailTo.add(email);
            SendTestEmail sendTestEmail = new SendTestEmail();
            sendTestEmail.setEmailTo(emailTo);
            api.sendTestTemplate(templateId, sendTestEmail);

            return true;
        } catch (ApiException e) {

            logger.info(LOGGER_MESSAGE);
            logger.error(" {}", e.getResponseBody());
            return false;

        } catch (Exception ex) {
            logger.error("{}", ex.getMessage());
            return false;
        }

    }

    @Override
    public GetSmtpTemplateOverview getEmailTemplate(long templateId, String sendInBuleKey) {

        apiKey.setApiKey(sendInBuleKey);

        try {
            TransactionalEmailsApi api = new TransactionalEmailsApi();
            return api.getSmtpTemplate(templateId);
        } catch (Exception ex) {
            logger.error("{}", ex.getMessage());
        }
        return null;

    }

}
