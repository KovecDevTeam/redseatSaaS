package com.SaaS.redseat.kovecmedia.redseatSaaS.payload;

import java.io.Serializable;
import java.util.List;

import com.SaaS.redseat.kovecmedia.redseatSaaS.constant.StautsCode;

public class Payload<T extends Serializable> {

    private boolean status;

    private StautsCode code;

    private String message;

    List<T> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public StautsCode getCode() {
        return code;
    }

    public void setCode(StautsCode code) {
        this.code = code;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
}
