package com.SaaS.redseat.kovecmedia.redseatSaaS.publisher;

import com.SaaS.redseat.kovecmedia.redseatSaaS.event.UserRegisterEvent;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request.UserRegistrationRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class UserRegisterEventPublisher {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publishEvent(UserRegistrationRequest user) {
        applicationEventPublisher.publishEvent(new UserRegisterEvent(user));
    }

}
