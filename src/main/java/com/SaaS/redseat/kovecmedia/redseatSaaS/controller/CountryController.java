package com.SaaS.redseat.kovecmedia.redseatSaaS.controller;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Country;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.Payload;
import com.SaaS.redseat.kovecmedia.redseatSaaS.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/country")
public class CountryController {
    
    @Autowired
    CountryService countryService;
    
    @GetMapping(value = "/", produces = "application/json")
    public  @ResponseBody Payload<Country> index() {
      return countryService.getAllCounty();
    }

}
