package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Delivery;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DeliveryRepo  extends JpaRepository<Delivery, Long> {
    
}