package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.ExRateHistory;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExRateHistoryRepo extends JpaRepository<ExRateHistory,Long> {
    
}
