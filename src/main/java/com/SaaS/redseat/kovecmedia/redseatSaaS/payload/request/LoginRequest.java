package com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request;

import java.io.Serializable;

public class LoginRequest implements Serializable {
    
   
	private String email;
    
	private String password;

	private long companyId;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	
}
