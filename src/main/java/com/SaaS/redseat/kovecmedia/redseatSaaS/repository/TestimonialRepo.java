package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Testimonial;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TestimonialRepo  extends JpaRepository<Testimonial, Long> {
    
}