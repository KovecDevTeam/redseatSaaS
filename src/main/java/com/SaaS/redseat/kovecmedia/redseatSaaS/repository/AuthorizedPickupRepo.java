package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.AuthorizedPickup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorizedPickupRepo  extends JpaRepository<AuthorizedPickup, Long> {
    
}