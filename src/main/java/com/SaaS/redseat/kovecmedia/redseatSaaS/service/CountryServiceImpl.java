package com.SaaS.redseat.kovecmedia.redseatSaaS.service;

import com.SaaS.redseat.kovecmedia.redseatSaaS.constant.StautsCode;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Country;
import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.Payload;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.CountryRepo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CountryServiceImpl implements CountryService {

    Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);
     
    @Autowired
    CountryRepo countryRepo;

    @Override
    public Payload<Country> getAllCounty(){
       
        Payload<Country> countryPayload = new Payload<>();
        try {
            countryPayload.setStatus(true);
            countryPayload.setCode(StautsCode.RCS_101);
            countryPayload.setData(countryRepo.findAll());
            
            return countryPayload;
        } catch (Exception e) {

            logger.error(e.getMessage());
           
            countryPayload.setStatus(false);
            countryPayload.setCode(StautsCode.RCS_102);
            countryPayload.setData(null);
            return countryPayload;
        }

      
    }

}
