package com.SaaS.redseat.kovecmedia.redseatSaaS.seeder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Address;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Company;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Country;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Role;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Warehouse;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.AddressRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.CompanyRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.CountryRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.RoleRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.UserRepo;
import com.SaaS.redseat.kovecmedia.redseatSaaS.repository.WareHouseRepo;
import com.github.javafaker.Faker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {

	static final long DEFAULT_ID = 1L;

	Logger logger = LoggerFactory.getLogger(DataLoader.class);

	private UserRepo userRepo;

	private CountryRepo countryRepo;

	private RoleRepo roleRepo;

	private AddressRepo addressRepo;

	private WareHouseRepo wareHouseRepo;

	private CompanyRepo companyRepo;

	public DataLoader(UserRepo userRepository, CountryRepo countryRepo, RoleRepo roleRepo, AddressRepo addressRepo,
			WareHouseRepo wareHouseRepo, CompanyRepo companyRepo) {
		this.userRepo = userRepository;
		this.countryRepo = countryRepo;
		this.roleRepo = roleRepo;
		this.addressRepo = addressRepo;
		this.wareHouseRepo = wareHouseRepo;
		this.companyRepo = companyRepo;
	}

	public void run(ApplicationArguments args) {
		loadCounties();
		loadRoles();
		loadAddresses();
		loadWareHouses();
		loadCompanies();
	}

	public void loadCounties() {
		try {
			List<Country> list = new ArrayList<>();
			Faker faker = new Faker();
			NumberFormat formatter = new DecimalFormat("#0.00000");

			long start = System.currentTimeMillis();
			for (int i = 1; i <= 200; i++) {
				Country country = new Country();
				country.setCode(faker.country().countryCode2());
				country.setCountryCode(faker.country().countryCode3());
				country.setCountryName(faker.country().name());
				country.setUpdateBy("system");
				list.add(country);
			}
			long end = System.currentTimeMillis();
			this.countryRepo.saveAll(list);
			logger.info("Country Seeder ran in {}", formatter.format((end - start) / 1000d) + " seconds");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void loadRoles() {

		try {
			List<Role> list = new ArrayList<>();

			NumberFormat formatter = new DecimalFormat("#0.00000");

			long start = System.currentTimeMillis();

			Role role1 = new Role();
			Role role2 = new Role();
			Role role3 = new Role();

			role1.setDisplayName("Admin");
			role1.setName("Admin");

			role2.setDisplayName("User");
			role2.setName("User");

			role3.setDisplayName("Manager");
			role3.setName("Manager");

			list.add(role1);
			list.add(role2);
			list.add(role3);

			long end = System.currentTimeMillis();
			this.roleRepo.saveAll(list);
			logger.info("Role Seeder ran in {}", formatter.format((end - start) / 1000d) + " seconds");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void loadAddresses() {

		try {
			List<Address> list = new ArrayList<>();
			Faker faker = new Faker();
			NumberFormat formatter = new DecimalFormat("#0.00000");

			long start = System.currentTimeMillis();
			for (int i = 1; i <= 10; i++) {
				Address address = new Address();
				address.setAddressline1(faker.address().streetAddress());
				address.setAddressline2(faker.address().secondaryAddress());
				address.setCountry(this.countryRepo.getById(DEFAULT_ID));
				address.setZipcode(faker.address().zipCode());
				address.setUpdatedBy("system");
				list.add(address);
			}
			long end = System.currentTimeMillis();
			this.addressRepo.saveAll(list);
			logger.info("Address  Seeder ran in " + formatter.format((end - start) / 1000d) + " seconds");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void loadWareHouses() {

		try {
			List<Warehouse> list = new ArrayList<>();
			Faker faker = new Faker();
			NumberFormat formatter = new DecimalFormat("#0.00000");

			long start = System.currentTimeMillis();
			for (int i = 1; i <= 10; i++) {
				Warehouse warehouse = new Warehouse();
				HashSet<Address> addresslist = new HashSet<>();
				addresslist.add(this.addressRepo.getById((long) i));
				warehouse.setName(faker.name().fullName());
				warehouse.setAddress(addresslist);
				list.add(warehouse);
			}
			long end = System.currentTimeMillis();
			this.wareHouseRepo.saveAll(list);
			logger.info("Warehouse  Seeder ran in " + formatter.format((end - start) / 1000d) + " seconds");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void loadCompanies() {

		try {
			List<Company> list = new ArrayList<>();
			Faker faker = new Faker();
			NumberFormat formatter = new DecimalFormat("#0.00000");

			long start = System.currentTimeMillis();
			for (int i = 1; i <= 10; i++) {
				Company company = new Company();
			    company.setName(faker.app().name());
				company.setCode(faker.business().creditCardType());
				company.setCompanyEmail(faker.company().industry());
				company.setSenderEmail(faker.company().logo());
				company.setCompanyWarehouse(this.wareHouseRepo.getById((long)i));
				list.add(company);
			}
			long end = System.currentTimeMillis();
			this.companyRepo.saveAll(list);
			logger.info("Company  Seeder ran in " + formatter.format((end - start) / 1000d) + " seconds");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}