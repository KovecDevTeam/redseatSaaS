package com.SaaS.redseat.kovecmedia.redseatSaaS.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.SaaS.redseat.kovecmedia.redseatSaaS.constant.ShippingType;

@Entity(name = "packages")
// @Table(name = "packages")
public class Parcel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	private String trackingNumber;

	private String description;

	private String reason;

	private String seller;

	private long weight;

	private double value;

	private ShippingType type;

	@OneToOne(fetch = FetchType.EAGER)
	private PackageStatus status;

	private String invoceUrl;

	private boolean preAlert;

	@OneToOne(fetch = FetchType.EAGER)
	private Company company;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public long getWeight() {
		return weight;
	}

	public void setWeight(long weight) {
		this.weight = weight;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getInvoceUrl() {
		return invoceUrl;
	}

	public void setInvoceUrl(String invoceUrl) {
		this.invoceUrl = invoceUrl;
	}

	public boolean isPreAlert() {
		return preAlert;
	}

	public void setPreAlert(boolean preAlert) {
		this.preAlert = preAlert;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public PackageStatus getStatus() {
		return status;
	}

	public void setStatus(PackageStatus status) {
		this.status = status;
	}

	public ShippingType getType() {
		return type;
	}

	public void setType(ShippingType type) {
		this.type = type;
	}

	

}
