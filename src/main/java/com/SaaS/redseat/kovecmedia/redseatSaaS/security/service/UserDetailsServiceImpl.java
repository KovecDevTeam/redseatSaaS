
package com.SaaS.redseat.kovecmedia.redseatSaaS.security.service;
import java.util.Map;
import java.util.Map.Entry;

import com.SaaS.redseat.kovecmedia.redseatSaaS.constant.GlobalCache;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.User;
import com.SaaS.redseat.kovecmedia.redseatSaaS.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	UserService service;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		long CompanyId =0;
		Map<String,String> map = GlobalCache.cache;

        for (Entry < String, String > entry: map.entrySet()) {
			if(entry.getKey().equals("CompanyID")){
				CompanyId=Long.parseLong(entry.getValue());
			}
        }
		User user = service.findByEmailAndCompany(email,CompanyId)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + email));
		return UserDetailsImpl.build(user);
	}

}