package com.SaaS.redseat.kovecmedia.redseatSaaS.service;

import java.io.Serializable;

import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.Payload;

public interface PasswordResetService<T extends Serializable> {
    
    Payload<T> createResetRequest(String email,long companyID);

    boolean verfityResetCode(String code);

    Payload<T> changePassword(String code);

}
