package com.SaaS.redseat.kovecmedia.redseatSaaS.service.email;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.User;

import sibModel.GetSmtpTemplateOverview;

public interface SendinBuleAPI {

    boolean importContact(User user, String sendInBuleKey);

    boolean sendWelcomeEmail(long templateId, User email, String sendInBuleKey);

    boolean testEmailTemaple(long templateId, String email, String sendInBuleKey);

    GetSmtpTemplateOverview getEmailTemplate(long templateId, String sendInBuleKey);
}
