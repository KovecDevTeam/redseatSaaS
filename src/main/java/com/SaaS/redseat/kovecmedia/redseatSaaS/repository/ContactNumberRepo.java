package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.ContactNumber;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactNumberRepo extends JpaRepository<ContactNumber, Long> {
    
}
