package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.RateGroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RateGroupRepo extends JpaRepository<RateGroup, Long> {
    
}