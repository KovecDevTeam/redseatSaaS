package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Manifest;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ManifestRepo extends  JpaRepository<Manifest,Long> {
    
}
