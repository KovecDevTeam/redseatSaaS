package com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request;

import java.io.Serializable;

public class AuthorizedUsersRequest implements Serializable {
    
    String name;
	String idnumber;
	int userid;
	int idtype;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdnumber() {
		return idnumber;
	}
	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getIdtype() {
		return idtype;
	}
	public void setIdtype(int idtype) {
		this.idtype = idtype;
	}
}
