package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.PackageStatus;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ParcelRepo  extends JpaRepository<PackageStatus, Long> {
    
}