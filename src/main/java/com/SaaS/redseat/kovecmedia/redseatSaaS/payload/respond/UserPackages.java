package com.SaaS.redseat.kovecmedia.redseatSaaS.payload.respond;

import java.util.List;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Parcel;

public class UserPackages {
   
    
    private String name;
	private long id;
	private long points;
	
	private List<Parcel> packageslist;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public List<Parcel> getPackageslist() {
        return packageslist;
    }

    public void setPackageslist(List<Parcel> packageslist) {
        this.packageslist = packageslist;
    }
	

    
}
