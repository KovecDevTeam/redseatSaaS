package com.SaaS.redseat.kovecmedia.redseatSaaS.handler;

import com.SaaS.redseat.kovecmedia.redseatSaaS.event.UserRegisterEvent;
import com.SaaS.redseat.kovecmedia.redseatSaaS.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class UserRegisterEventHandler {

    @Autowired
    UserService service;

    @EventListener
    @Async
    void handleEvent(UserRegisterEvent event) {

        try {
            service.addUser(event.getUser());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
