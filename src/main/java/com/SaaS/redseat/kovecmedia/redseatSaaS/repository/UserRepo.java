package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import java.util.Optional;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Company;
import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * User repository for CRUD operations.
 */
@Repository
public interface UserRepo extends JpaRepository<User, Long> {

	Optional<User> findByEmail(String email);
	Boolean existsByEmail(String email);
    
    Optional<User> findByEmailAndCompany(String email,Company company);
	Boolean existsByEmailAndCompany(String email,Company company);
}
