package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Manifest;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ManifestStatusRepo extends  JpaRepository<Manifest,Long>  {
    
}
