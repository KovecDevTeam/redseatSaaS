package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.MenuItem;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuItemRepo  extends  JpaRepository<MenuItem,Long> {
    
}
