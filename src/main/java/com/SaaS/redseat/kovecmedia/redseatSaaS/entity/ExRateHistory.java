package com.SaaS.redseat.kovecmedia.redseatSaaS.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ExRateHistories")
public class ExRateHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private float exRate;
    
    @Column(columnDefinition = "INT(11)")
    private long exRateDate;

    @OneToOne(fetch = FetchType.EAGER)
    Currency currency;

    @OneToOne(fetch = FetchType.EAGER)
    private Company company;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getExRate() {
        return exRate;
    }

    public void setExRate(float exRate) {
        this.exRate = exRate;
    }

    public Company getCompany() {
        return company;
    }

    

    public long getExRateDate() {
        return exRateDate;
    }

    public void setExRateDate(long exRateDate) {
        this.exRateDate = exRateDate;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Column(name = "created_at", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")

    private java.sql.Timestamp createdAt;

    @Column(name = "updated_at", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")

    private java.sql.Timestamp updateAt;
}
