package com.SaaS.redseat.kovecmedia.redseatSaaS.event;

import com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request.UserRegistrationRequest;

import org.springframework.context.ApplicationEvent;


public class UserRegisterEvent extends ApplicationEvent {
   
    private UserRegistrationRequest user;

    public UserRegisterEvent(UserRegistrationRequest event) {
        super(event);
        this.user=event;
        //TODO Auto-generated constructor stub
    }

    public UserRegistrationRequest getUser() {
        return user;
    }

    public void setUser(UserRegistrationRequest user) {
        this.user = user;
    }

    
    
}
