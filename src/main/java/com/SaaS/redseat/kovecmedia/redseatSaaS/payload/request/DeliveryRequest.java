package com.SaaS.redseat.kovecmedia.redseatSaaS.payload.request;

import java.io.Serializable;
import java.util.List;

public class DeliveryRequest implements Serializable {

    private String instructions;
	private String peroid;
	private long date;
	private String addressLine1;
	private String addressLine2;
	private String zipCode;
	private List<Package> packageList;
	private float charge;
	private long userId;
    private long companyId;
    
    public String getInstructions() {
        return instructions;
    }
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
    public String getPeroid() {
        return peroid;
    }
    public void setPeroid(String peroid) {
        this.peroid = peroid;
    }
    public long getDate() {
        return date;
    }
    public void setDate(long date) {
        this.date = date;
    }
    public String getAddressLine1() {
        return addressLine1;
    }
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }
    public String getAddressLine2() {
        return addressLine2;
    }
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    public List<Package> getPackageList() {
        return packageList;
    }
    public void setPackageList(List<Package> packageList) {
        this.packageList = packageList;
    }
    public float getCharge() {
        return charge;
    }
    public void setCharge(float charge) {
        this.charge = charge;
    }
    public long getUserId() {
        return userId;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public long getCompanyId() {
        return companyId;
    }
    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

   
    
	
}
