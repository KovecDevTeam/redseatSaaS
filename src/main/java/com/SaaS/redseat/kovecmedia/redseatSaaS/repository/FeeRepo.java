package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.Fee;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FeeRepo extends JpaRepository<Fee,Long> {
    
}
