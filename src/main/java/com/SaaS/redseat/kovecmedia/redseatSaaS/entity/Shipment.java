package com.SaaS.redseat.kovecmedia.redseatSaaS.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "shipments")
public class Shipment {
    
    
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String receivedby;
	
	private String numberOfItems;

    private double totalWeigth;
    
    @OneToMany(fetch = FetchType.LAZY)
	private Set<User> customers;
	
	private java.sql.Timestamp dateReceived;
	
	@Column(name = "created_at", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	
	private java.sql.Timestamp createdAt;
	@Column(name = "updated_at", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private java.sql.Timestamp updatedAt;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getReceivedby() {
        return receivedby;
    }
    public void setReceivedby(String receivedby) {
        this.receivedby = receivedby;
    }
    public String getNumberOfItems() {
        return numberOfItems;
    }
    public void setNumberOfItems(String numberOfItems) {
        this.numberOfItems = numberOfItems;
    }
  
    public java.sql.Timestamp getDateReceived() {
        return dateReceived;
    }
    public void setDateReceived(java.sql.Timestamp dateReceived) {
        this.dateReceived = dateReceived;
    }
    public java.sql.Timestamp getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(java.sql.Timestamp createdAt) {
        this.createdAt = createdAt;
    }
    public java.sql.Timestamp getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(java.sql.Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }
    public double getTotalWeigth() {
        return totalWeigth;
    }
    public void setTotalWeigth(double totalWeigth) {
        this.totalWeigth = totalWeigth;
    }
    public Set<User> getCustomers() {
        return customers;
    }
    public void setCustomers(Set<User> customers) {
        this.customers = customers;
    }

}
