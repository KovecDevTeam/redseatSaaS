package com.SaaS.redseat.kovecmedia.redseatSaaS.repository;

import com.SaaS.redseat.kovecmedia.redseatSaaS.entity.ForgetPassword;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ForgetPasswordRepo extends JpaRepository<  ForgetPassword,Long> {
    
}
